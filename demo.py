#!/usr/bin/python

import time
import os

XSIZE=10
YSIZE=10

#directions
RIGHT, UP, LEFT, DOWN = 0,1,2,3

def movable(x, y):
    return x<XSIZE-1 and x>0 and y<YSIZE-1 and y>0

def createBoard(w,h):
    b=[]
    for x in range(w):
        b.append([])
        for y in range(h):
            b.append('.')
    return b

def drawBoard(brd,rbt):
    os.system("clear")
    for y in range(YSIZE):
        b=''
        for x in range(XSIZE):
            if x==rbt.x and y==rbt.y:
                b+=rbt.char
            else:
                b+='.'
        print b


class Robot:
    def __init__(self,ix, iy):
        self.x=ix
        self.y=iy
        self.direction=UP
        self.char='^'

    def handleCommand(self, command):
        if command == 'l':  #turn left
            self.direction+=1
            self.direction%=4
            self.updateChar()
        elif command == 'r':  #turn right
            self.direction+=4
            self.direction-=1
            self.direction%=4
            self.updateChar()
        elif command == 'f' or command == '1' or command == '2' or command == '3':
            if command == 'f':
                command = '1'
            #move forward
            amount = int(command)
            for i in range(amount):
                self.move(forward=True)
        elif command == 'b':
            self.move(forward=False)

        print "direction: %d x:%d y:%d"%(self.direction, self.x, self.y)



    def move(self,  forward = True):
        if forward:
            inc=1
        else:
            inc=-1

        print self.direction, forward

        if self.direction == RIGHT:
            if movable(self.x+inc, self.y):
                self.x+=inc
        elif self.direction == UP:
            if movable(self.x, self.y-inc):
                self.y-=inc
        elif self.direction == LEFT:
            if movable(self.x-inc, self.y):
                self.x-=inc
        elif self.direction == DOWN:
            if movable(self.x, self.y+inc):
                self.y+=inc

    def updateChar(self):
        if self.direction == UP:
            self.char='^'
        if self.direction == LEFT:
            self.char='<'
        if self.direction == DOWN:
            self.char='v'
        if self.direction == RIGHT:
            self.char='>'




def readCommand():
    print "Input command: ",
    cmd=raw_input()
    cmd=cmd.lower()
    return cmd
            
        

def main():
    xpos, ypos = 5,5
    board=createBoard(XSIZE, YSIZE)
    time.sleep(1)
    robot = Robot(xpos, ypos)
    drawBoard(board,robot)
    print ' A command is a string consisting of f,1,2,3,l,r,b'
    print ' (F)orward, (B)ackward, turn (L)eft or turn (R)right'
    while True:
        command=readCommand()
        for i in range(len(command)):
            robot.handleCommand(command[i])
            drawBoard(board,robot)
            time.sleep(1)






main()

